#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include "hd44780.h"
#include "dht11.h"
#define F_CPU 16000000L //tu wstaw wartość
#define INT_PIN 2

bool temp_or_humi = true;
char data[5];

ISR(INT0_vec){
  temp_or_humi = !temp_or_humi;

  if (temp_or_humi) {
    temperature();
  }
  else {
    humidity();
  }
}

// funkcja termometra 
void temperature(){
    // blok obsługi sensora DHT11
    Request();
    Response();
    I_humidity = Receive_data();
    D_humidity = Receive_data();
    I_temperature = Receive_data();
    D_temperature = Receive_data();
    Check_sum = Receive_data();

    if((I_humidity + D_humidity + I_temperature + D_temperature) != Check_sum){
        temperature();
    }
    else {
        // blok obsługi wyświetlacza 
        LcdClear();
        Lcd1;
        LcdWrite("Temperature:");
        Lcd2;

        itoa(I_temperature, data, 10);
        LcdWrite(data);
        LcdWrite(".");

        itoa(D_temperature, data, 10);
        LcdWrite(data);
        LcdData(" deg C");
  }
}

// funkcja wilgotnościomierza 
void humidity(){
    // obsługa czujnika DHT11
    Request();
    Response();
    I_humidity = Receive_data();
    D_humidity = Receive_data();
    I_temperature = Receive_data();
    D_temperature = Receive_data();
    Check_sum = Receive_data();

    if((I_humidity + D_humidity + I_temperature + D_temperature) != Check_sum){
        humidity();
    }
    else{
        // obsługa wyświetlacza 
        LcdClear();
        Lcd1;
        LcdWrite("Humidity:")
        Lcd2;

        itoa(I_humidity, data, 10);
        LcdWrite(data);
        LcdWrite(".");

        itoa(D_humidity, data, 10);
        LcdWrite(data);
        LcdWrite("%");
  }
}

int main(){
    // włączenie pinu przerwania na input
    DDRD &= ~(1<<INT_PIN);
    // podciągnięcie pull up pinu przerwania
    PORTD |= (1<<INT_PIN);
    
    // jeszcze ustawienie rejestrów EICRA i EIFR
    // ISC00 pozwala na to, żeby zmiana przerwanie było wyzwalane stanem niskim
    // jeżeli dobrze rozumiem
    EICRA |= (1<<ISC00);
    EIFR |= (1<<INT0);
    
    // zainicjalizowanie wyświetlacza
    LcdInit();
    // włączenie obsługiwania przewań
    sei();
  
    while(1){

    if(temp_or_humi){
        temperature();
        _delay_ms(1500);
    }
    else{
        humidity();
        _delay_ms(1500);
    }

  }
}
