#ifndef DHT11_H
#define DHT11_H

#define SENSOR_DDR DDRD
#define SENSOR_PORT PORTD
#define SENSOR_PIN 6 // digital pin 6

uint8_t c = 8;
uint8_t I_humidity, D_humidity, I_temperature, D_temperature, Check_sum;

void Request();
void Response();
uint8_t Receive_data();

#endif
