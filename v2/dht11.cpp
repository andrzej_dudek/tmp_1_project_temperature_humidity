#include "dht11.h"
#include <avr/io.h>
#include <util/delay.h>
#define F_CPU 16000000L

uint8_t c = 0;

void Request(){
  SENSOR_DDR |= (1<<SENSOR_PIN); // set ddr for sesor pin 1
  SENSOR_PORT &= ~(1<<SENSOR_PIN); // set pin to low for 20 ms
  _delay_ms(20);
  SENSOR_PORT |= (1<<SENSOR_PIN); // set pin to high
}

void Response(){
  // receive response from dht 11 sensor
  SENSOR_DDR &= ~(1<<SENSOR_PIN);
  while(PIND & (1<<SENSOR_PIN));
  while((PIND & (1<<SENSOR_PIN)) == 0);
  while(PIND & (1<<SENSOR_PIN));
}

uint8_t Receive_data(){
  for (int q=0; q<8; q++)
	{
		while((PIND & (1<<SENSOR_PIN)) == 0);  /* check received bit 0 or 1 */
		_delay_us(30);
		if(PIND & (1<<SENSOR_PIN))/* if high pulse is greater than 30ms */
		c = (c<<1)|(0x01);	/* then its logic HIGH */
		else			/* otherwise its logic LOW */
		c = (c<<1);
		while(PIND & (1<<SENSOR_PIN));
	}
	return c;
}
