#ifndef LCD16X2_4BIT_H
#define LCD16X2_4BIT_H

#define F_CPU 16000000L
#include <avr/io.h>
#include <util/delay.h>

#define LCD_DDR DDRB
#define LCD_PORT PORTB
#define RS PB5
#define EN PB4

// pamiętaj że tutaj wysyłasz na dolny nibble dane i komendy

void LCD_Command(unsigned char cmnd);
void LCD_Char(unsigned char data);
void LCD_Init(void);
void LCD_String(char *str);
void LCD_chartab(char *tab, unsigned int size);
void LCD_XY(char row, char pos);
void LCD_Clear();

#endif
