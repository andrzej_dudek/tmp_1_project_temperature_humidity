#include "lcd16x2_4bit.h"

void LCD_Command(unsigned char cmnd){
  LCD_PORT = (LCD_PORT & 0xF0) | (cmnd & 0xF0);
  LCD_PORT &= ~(1<<RS);
  LCD_PORT |= (1<<EN);
  _delay_us(1);
  LCD_PORT &= ~(1<<EN);
  _delay_us(200);
  LCD_PORT = (LCD_PORT & 0xF0) | (cmnd << 4);
  LCD_PORT |= (1<<EN);
  _delay_us(1);
  LCD_PORT &= ~(1<<EN);
  _delay_ms(2);
}

void LCD_Char(unsigned char data){
  LCD_PORT = (LCD_PORT & 0xF0) | (data & 0xF0);
  LCD_PORT |= (1<<RS);
  LCD_PORT |= (1<<EN);
  _delay_us(1);
  LCD_PORT &= ~(1<<EN);

  _delay_us(200);

  LCD_PORT = (LCD_PORT & 0xF0) | (data << 4);
  LCD_PORT |= (1<<EN);
  _delay_us(1);
  LCD_PORT &= ~(1<<EN);
  _delay_ms(2);
}

void LCD_Init(void){
  LCD_DDR = 0xFF;
  _delay_ms(20);

  LCD_Command(0x02); // wyślij 4 bitową inicjalizację
  LCD_Command(0x28); // 2 liniowa macierz 5x7 w 4-bitowym modzie
  LCD_Command(0x0c); // Wyłącz wyświetlacz wyłącz kursor
  LCD_Command(0x06); // Przesuń kursor w prawo
  LCD_Command(0x01); // Wyczyść wyświetlacz
  _delay_ms(2);
}

void LCD_String(char *str){
  unsigned int i;

  for(i=0; str[i]!=0, i++){
    LCD_Char(str[i]);
  }
}

void LCD_chartab(char *tab, unsigned int size){
  unsigned int i;

  for(i=0; i=<size; i++){
     LCD_Char(tab[i]);
  }
}

void LCD_XY(char row, char pos){
  if (row == 0 && pos < 16){
    LCD_Command((pos & 0x0F) | 0x80);
  }
  else if (row == 1 && pos < 16){
    LCD_Command((pos & 0x0F) | 0xC0);
  }
}

void LCD_Clear(){
  LCD_Command(0x01);
  _delay_ms(2);
  LCD_Command(0x80);
}
