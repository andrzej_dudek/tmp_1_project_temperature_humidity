#ifndef DHT11_H_
#define DHT11_H_

#define SENSOR_DDR DDRD
#define SENSOR_PORT PORTD
#define SENSOR_PIN 6 // digital pin 6

#include <avr/io.h>

void Request();
void Response();
uint8_t Receive_data();

#endif